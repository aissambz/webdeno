import { serve } from "https://deno.land/std@0.114.0/http/server.ts";


const BOOK_ROUTE = new URLPattern({ pathname: "/books/:id" });
const HOME_ROUTE = new URLPattern({ pathname: "/" });

function handler(req: Request): Response {

  const matchHome = HOME_ROUTE.exec(req.url);
  const match = BOOK_ROUTE.exec(req.url);
  if (match) {
    const id = match.pathname.groups.id;
    return new Response(`Book ${id}`);
  } else if (matchHome) {
    return new Response(`Welcome home`);
  }

  return new Response("Not found (try /books/1)", {
    status: 404,
  });
}

serve(handler);
