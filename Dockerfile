FROM denoland/deno

EXPOSE 8000

WORKDIR /app

ADD . /app

RUN deno cache bookserv.ts

CMD ["run", "--allow-net", "bookserv.ts"]
